# Noyau IA Battle

Le but de ce projet est de proposé des "noyaux" de différent jeu simple tels que l'othello (reversi), ou encore le puissance 4, l'atari go ...
Pour ensuite organiser des petits championnats entre bot.

## Le jeu d'othello

Il s'agit du premier jeu implémenté pour ce projet.
Vous pouvez le retrouver dans le dossier othello.

Vous y trouverais un nouveau readme, qui explique comment créer votre bot python pour pouvoir le confronté aux autres.

## Remerciement

Je tiens à remercier Guillaume D. qui a grandement participé à l'élaboration du noyau pour le jeu d'othello.
Mais aussi tous ceux qui vont s'amuser à créer des bots pour les faire affronter.

## Suggestions

Si vous avez une suggestion, n'hésitez pas à me la faire parvenir.

Merci