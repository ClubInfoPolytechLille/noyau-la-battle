# Structure de votre bot

Il s'agit d'une classe du nom de votre bot.
Elle doit hérité de IAModels.
Il doit avoir obligatoirement 1 méthode:
 - jouer(self) qui renvoie un chiffre entre 1 et 7 qui indique ou jouer son pion 
 
# Explication fichier

## iaModels

il s'agit en faite d'une classe mère.
Vos bot vont hériter de cette classe.
Cela permet de les obliger a avoir la fonction jouer et de mettre a jour sans que vous vous en souciez les différents parametre du jeu comme l'etat du plateau.

## ialea

il s'agit d'un bot qui joue aléatoire, idéal pour tester vos premiers bots et comprendre la stucture de communication avec le noyau.

## jeu

le noyau, il dispose des règles du jeu de puissance 4, et c'est lui qui s'occupe de la communication.
Jetez y un oeil ;)

## puissance4x100

permet de lancer 100 matchs entre 2 bots.
Modifier le à votre guise ;)

## TODO

- [ ] contrainte de temps
- [ ] fichier de mémorisation des parties
- [ ] visualisation des parties

**N'hésitez pas a poser vos questions**
