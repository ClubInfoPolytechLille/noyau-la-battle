# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
import copy
class IAModel:
    nombreVictoire = 0
    nombreDefaite = 0
    nombreNul = 0
    colJouable = []
    plateau = [ [ 0 for i in range(9) ] for j in range(8) ] #ligne puis colonne    listeJouable = []
    nombreTour = 0

    
    def __init__(self, id_joueur) : 
        """
        fonction obligatoire
        """
        self.joueur = id_joueur
        
    
    @abstractmethod
    def jouer(self):
        raise NotImplementedError()
        
        
    def miseAJour(self, colPleine, matrice, nombreTour):
        """
        fonction obligatoire
        """
        self.plateau = copy.deepcopy(matrice)
        self.nombreTour = nombreTour
        self.colJouable = colPleine.copy()
        
    
    def enregistrerScore(self,idVainqueur):
        """
        fonction obligatoire
        """
        if idVainqueur == self.joueur:
            self.nombreVictoire += 1
        elif (idVainqueur == 0):
            self.nombreNul +=1
        else:
            self.nombreDefaite +=1
