# -*- coding: utf-8 -*-
from random import randint
from iaModel import IAModel
class IAalea(IAModel):
    

    def jouer(self):
        """
        fonction obligatoire
        """
        listeJouable = []
        for i in range(len(self.colJouable)):
            if self.colJouable[i] == False:
                listeJouable.append(i)
        choix = randint(0, len(listeJouable)-1)
        return listeJouable[choix]+1
