# -*- coding: utf-8 -*-
#c'est partie

class Jeu:
    
    def __init__(self, ia1, ia2, num):
        
        
        self.plateau =[ [ 0 for i in range(9) ] for j in range(8) ] #ligne puis colonne
        self.colPleine = [False for i in range(7)]
        self.vainqueur = 0
        self.joueur = 1             #1 pour le joueur 1, 2 pour l'autre
        self.listeIA = [ia1, ia2]   #liste des IAs
        self.nombreTour = 0         #nombre de tour
        
        #self.codePartie = ""        #voir encodage partie pour comprendre un peu
        self.numero = num           #numero de la partie, pour pas en oublier une
        
    def opposant(self):
        """
        Application concrete du cours de maths discrete.
        Permet de changer le joueur facilement.
        """
        return self.joueur %2 +1
    
    def finPartie(self):
        return (False not in self.colPleine or self.vainqueur>0)
        
    def placerPion(self,coup):
        for i in range(5):
            if self.plateau[i+1][coup] == 0:
                self.plateau[i+1][coup] = self.joueur
                return (i+1,coup)
        if self.plateau[6][coup] == 0:
            self.plateau[6][coup] = self.joueur
            self.colPleine[coup-1] = True
            return (6,coup)
        
    def victoire(self,coord):
        cpt = 1
        #gauche droite
        i=1
        while self.plateau[coord[0]][coord[1]-i] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur
        i=1
        while self.plateau[coord[0]][coord[1]+i] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur
        #bas haut
        cpt = 1
        i=1
        while self.plateau[coord[0]-i][coord[1]] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur
        i=1
        while self.plateau[coord[0]+i][coord[1]+i] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur
        #Nord Ouest - Sud est #cote d'azure
        cpt = 1
        i=1
        while self.plateau[coord[0]+i][coord[1]-i] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur
        i=1
        while self.plateau[coord[0]-i][coord[1]+i] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur
        #Sud ouest - Nord est #Metz
        cpt = 1
        i=1
        while self.plateau[coord[0]-i][coord[1]-i] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur
        i=1
        while self.plateau[coord[0]+i][coord[1]+i] == self.joueur:
            cpt+=1
            i+=1
            if cpt>=4:
                self.vainqueur=self.joueur

    
    def start(self):
        """
        Fonction qui s'occupe du déroulement du jeu
        """    
        while  self.finPartie() == False:

            self.nombreTour +=1
            self.listeIA[self.joueur-1].miseAJour(self.colPleine,self.plateau,self.nombreTour)
            coup = self.listeIA[self.joueur-1].jouer()
            if (coup<1 and coup>7) or self.colPleine[coup-1] :
                print("je triche")
            #self.encodagePartie(coup,self.joueur)
            coord = self.placerPion(coup)
            self.victoire(coord)
            self.joueur = self.opposant()
        return self.vainqueur
    
    
#    def encodagePartie(self, coordJouer, id_joueur):
#        """
#        La partie est codé selon ce format 1c4:2b6:1b7..._1
#        avec numero de ligne sous forme de caractere
#        et numero de colonne concatné
#        """
#        chaineCar = "abcdefgh"
#        ligne = coordJouer[0]
#        colonne = coordJouer[1]
#        self.codePartie += str(id_joueur) + chaineCar[ligne] + str(colonne) +":"
        
#    def ecrirePartie(self,score1,score2):
#        if score1 > score2 :
#            victorieux = "1_"
#        elif score1 < score2:
#            victorieux = "2_"
#        else:
#            victorieux = "0_"
#        with open("memoire/data.txt", "a") as fichier:
#            fichier.write(str(self.numero)+":"+victorieux +self.codePartie+"\n" )
#        self.codePartie = ""
            
            
            
            
            
            
            
            
        
            
            
        
        
        
        
        
    
