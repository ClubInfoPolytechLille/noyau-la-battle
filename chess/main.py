#!/usr/bin/env python3
########################################################################
# JeffProd Simple Python Chess Program
########################################################################
# AUTHOR	: Jean-Francois GAZET
# WEB 		: http://www.jeffprod.com
# TWITTER	: @JeffProd
# MAIL		: jeffgazet@gmail.com
# LICENCE	: GNU GENERAL PUBLIC LICENSE Version 2, June 1991
########################################################################

# Import object classes
from board import *
from engine import *

b=Board()
e=Engine()


while(True):

    #b.render()


    c=input()

    #NON UCI
    if(c=='quit' or c=='exit'):
        exit(0)

    #NON UCI
    elif(c=='undomove'):
        e.undomove(b)

    #NON UCI
    elif('setboard' in c):
        e.setboard(b,c)

    #NON UCI
    elif(c=='viewboard'):
        b.render()

    #NON UCI
    elif(c=='getboard'):
        e.getboard(b)

    elif('go' in c):
        temps = 600000
        if ('wtime' in c):
            t = c.split()
            if b.side2move == 'blanc':
                temps = int(t[t.index('wtime')+1])
            else:
                temps = int(t[t.index('btime')+1])
        #7 seconde
        if temps <7000:
            e.init_depth = 2
        #1 min
        elif temps<60000:
            e.init_depth = 3
        #17 min
        elif temps<1020000:
            e.init_depth = 4
        #2h
        elif temps<7200000:
            e.init_depth = 5
        else:
            e.init_depth = 6
            
        print('bestmove',e.search(b))

    elif(c=='ucinewgame'):
        b.load_openings_book()
        e.newgame(b)

    #NON UCI
    elif(c=='bench'):
        e.bench(b)

    #NON UCI
    elif('sd ' in c):
        e.setDepth(c)

    #NON UCI
    elif('perft ' in c):
        e.perft(c,b)

    #NON UCI
    elif(c=='legalmoves'):
        e.legalmoves(b)

    elif(c=='uci'):
        b.load_openings_book()
        print('id name Wax')
        print('id author Nicogito')
        print('registration ok')
        print('uciok')


    elif(c=='isready'):
        print('readyok')

    elif('position startpos' in c):
        t = c.split()
        e.usermove(b,t[len(t)-1])
# =============================================================================
#
#     else:
#         # coup à jouer ? ex : e2e4
#         e.usermove(b,c)
#
# =============================================================================
