# -*- coding: utf-8 -*-
from random import randint
from jeu import Jeu
from ialea import IAalea
from timekeeper import globalStatsTime
#Ajouter votre i ici
import sys
import os

#Options d'affichages
WITH_PROGRESS_BAR=True
PGSBAR_KEEP_ON_COMPLETE=True

# Print iterations progress
if WITH_PROGRESS_BAR:
    def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r",chaine = ""):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
            printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
        """
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{chaine}{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
        # Print New Line on Complete
        if iteration == total:
            if PGSBAR_KEEP_ON_COMPLETE :
                print()
            else:
                toClear = length + len(chaine) + len(suffix) + len(prefix) + 7 + (decimals + 3)
                print("\r" + " "*(toClear))
else:
    def printProgressBar(iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
        pass

SHOW_TIME_GSTATS = True

"""

Bonne chance ;)

"""
os.makedirs("memoire", exist_ok=True)
ia2 = IAalea(2)
ia1 = IAalea(1)
nbVictoire = [0,0]
for i in range (0,100) :
    reversi = Jeu(ia1, ia2,i)
    reversi.start()
    score = reversi.score()
    if score[0]>score[1] :
        nbVictoire[0] = nbVictoire[0]+1
    elif score[0]<score[1] :
        nbVictoire[1] =nbVictoire[1]+1
    constructionChaine = str(ia1.__class__.__name__) +" : "+ str(nbVictoire[0])+ " | "+str(ia2.__class__.__name__)+" : "+ str(nbVictoire[1]) + " | "
    printProgressBar(i, 99, prefix = 'Progress:', suffix = 'Complete', length = 50,chaine=constructionChaine)


if SHOW_TIME_GSTATS :
    print("-----------")
    globalStatsTime()


             
                        
            
            
            
        
    
    
        
    

            
                
# 


             
                        
            
            
            
        
    
    
        
    

            
                
# DO NOT CHANGE THIS INSTRUCTION
