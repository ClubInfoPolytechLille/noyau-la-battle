# -*- coding: utf-8 -*-
from random import randint
from iaModel import IAModel
class IAalea(IAModel):
    
    """
    A votre disposition
        
    nombreVictoire : int
    nombreDefaite : int
    nombreNul : int
    plateau : Array2D(8,8)
    listeJouable : liste sous la forme ex [(coordx1,coordy1),(coordx2,coordy2),(coordx3,coordy3)...]
    nombreTour : int
    """
    def jouer(self):
        """
        fonction obligatoire
        """
        choix = randint(0, len(self.listeJouable)-1)
        return self.listeJouable[choix]
