# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from array2d import Array2D
class IAModel:
    nombreVictoire = 0
    nombreDefaite = 0
    nombreNul = 0
    plateau = Array2D(8,8)
    listeJouable = []
    nombreTour = 0

    
    def __init__(self, id_joueur) : #matrice,joueur, listeJouable, listePrevision,score,coupAdverse):
        """
        fonction obligatoire
        """
        self.joueur = id_joueur
        
    def copierMatrice(self,matriceACopier):
        matrice = Array2D(8,8)
        for i in range(8):
            for j in range(8):
                matrice.setvalue(i, j, matriceACopier.getvalue(i, j))
        return matrice
    
    @abstractmethod
    def jouer(self):
        raise NotImplementedError()
        
        
    def miseAJour(self, listeJouable, matrice, nombreTour):
        """
        fonction obligatoire
        """
        self.plateau = self.copierMatrice(matrice)
        self.nombreTour = nombreTour
        self.listeJouable = listeJouable.copy()
        
    
    def enregistrerScore(self,score1,score2):
        """
        fonction obligatoire
        """
        if (self.joueur == 1 and score1> score2) or (self.joueur == 2 and score2> score1):
            self.nombreVictoire += 1
        elif (score1 == score2):
            self.nombreNul +=1
        else:
            self.nombreDefaite +=1
