# -*- coding: utf-8 -*-
#c'est partie
from array2d import Array2D
from timekeeper import *

#constraints
MAX_TURN_TIME = 15 # ms

class Jeu:
    
    def __init__(self, ia1, ia2, num):
        
        self.plateau = Array2D(8,8) #Une matrice 8,8
        self.joueur = 1             #1 pour le joueur 1, 2 pour l'autre
        self.listeIA = [ia1, ia2]   #liste des IAs
        self.nombreTour = 0         #nombre de tour
        self.codePartie = ""        #voir encodage partie pour comprendre un peu
        self.numero = num           #numero de la partie, pour pas en oublier une
        
    def opposant(self):
        """
        Application concrete du cours de maths discrete.
        Permet de changer le joueur facilement.
        """
        return self.joueur %2 +1
        
    def placerPion(self, listeCoord,joueur):
        """
        Place les pions aux coordonnées données par la listeCoord
        Ex de listeCoord : [(3,3),(4,3),(4,4)]
        """
        for coord in listeCoord :
            self.plateau.setvalue(coord[0],coord[1],joueur)
    
    def getPionJoueur(self):
        """
        Permet d'obtenir la liste des pions du joueur actuel
        le get c'est juste pour faire bilingue
        """
        res = []
        for y in range(self.plateau.getdimone()):
            for x in range (self.plateau.getdimtwo()):
                if self.plateau.getvalue(y,x) == self.joueur:
                    res.append((y,x))
        return res
    
    def getPrevision(self):
        """
        Jolie fonction qui n'a plus besoin de Thread car ça servait à rien en fait
        elle renvoie la liste des chemins de prises et la liste des coups jouables
        Ex listeJouable : [(3,3),(4,3),(4,4)]
        Ex listePrevision : [[(2,3),(3,3)],[(4,5),(4,4)],[(3,2),(4,3)]]
        """   
        listePion = self.getPionJoueur()
        listeRes = []
        listeJouable = []
        for i1 in range(-1,2):
            for i2 in range(-1,2):
                for pion in listePion :
                    listeChemin = []
                    y = i1
                    x = i2
                    while self.plateau.isvalue(pion[0] + y,pion[1] + x) and self.plateau.getvalue(pion[0] + y, pion[1] + x) == self.joueur%2+1:
                        listeChemin.append((pion[0] + y,pion[1] + x))
                        y += i1
                        x += i2
                        if self.plateau.isvalue(pion[0] + y,pion[1] + x) and self.plateau.getvalue(pion[0] + y, pion[1] + x) == 0:
                            if (pion[0] + y,pion[1] + x) not in listeJouable :
                                listeJouable.append((pion[0] + y,pion[1] + x))
                                listeChemin.append((pion[0] + y,pion[1] + x))
                                listeRes.append(listeChemin)
                            else :
                                ind = listeJouable.index((pion[0] + y,pion[1] + x))
                                listeRes[ind] = listeChemin + listeRes[ind]
        return listeRes, listeJouable
    
    def positionInitial(self):
        """
        Initialise le plateau
        """
        self.plateau.setZero()
        self.placerPion([(3,4),(4,3)],1)
        self.placerPion([(3,3),(4,4)],2)
        
    
    def start(self):
        """
        Fonction qui s'occupe du déroulement du jeu
        """    
        global MAX_TURN_TIME
        self.positionInitial()
        jouable=[True,True]
        while jouable[0] or jouable[1] :
            listePrevision,listeJouable = self.getPrevision()
            if len(listeJouable) > 0:
                self.nombreTour +=1
                jouable[self.joueur-1] = True
                beginTime(self.joueur-1) #Time begin
                self.listeIA[self.joueur-1].miseAJour(listeJouable,self.plateau,self.nombreTour)
                turnTime = endTime(self.joueur-1) #Time reading
                if turnTime > MAX_TURN_TIME :
                    print("trop long...")
                coup = self.listeIA[self.joueur-1].jouer()
                if coup not in listeJouable :
                    print("je triche")
                self.encodagePartie(coup,self.joueur)
                for prevision in listePrevision :
                    if prevision[-1] == coup :
                        self.placerPion(prevision, self.joueur)
            else:
                jouable[self.joueur-1] = False
            self.joueur = self.opposant()
    
    def score(self):
        """
        Fonction qui s'occupe du comptes des scores
        """
        scorej1=0
        scorej2=0
        for i in range(self.plateau.getdimone()):
            for j in range (self.plateau.getdimtwo()):
                if self.plateau.getvalue(i,j) == 1 :
                    scorej1 +=1
                elif self.plateau.getvalue(i,j) == 2 :
                    scorej2 +=1
        listeScore = [scorej1,scorej2]
        self.ecrirePartie(scorej1, scorej2)
        self.listeIA[1].enregistrerScore(scorej1,scorej2)
        self.listeIA[0].enregistrerScore(scorej1,scorej2)
        commitTime() #Time commit
        return scorej1,scorej2
    
    def encodagePartie(self, coordJouer, id_joueur):
        """
        La partie est codé selon ce format 1c4:2b6:1b7..._1
        avec numero de ligne sous forme de caractere
        et numero de colonne concatné
        """
        chaineCar = "abcdefgh"
        ligne = coordJouer[0]
        colonne = coordJouer[1]
        self.codePartie += str(id_joueur) + chaineCar[ligne] + str(colonne) +":"
        
    def ecrirePartie(self,score1,score2):
        if score1 > score2 :
            victorieux = "1_"
        elif score1 < score2:
            victorieux = "2_"
        else:
            victorieux = "0_"
        with open("memoire/data.txt", "a") as fichier:
            fichier.write(str(self.numero)+":"+victorieux +self.codePartie+"\n" )
        self.codePartie = ""
            
            
            
            
            
            
            
            
        
            
            
        
        
        
        
        
    
