
# this file is for static/global use

import time
from copy import deepcopy

#current measure
lastMeasure_player = -1 # flag for waiting
lastMeasure_beginTime = 0
#current game : array for each player, turn x is x-th (-1) element / one element = time spend in ms
player1 = []
player2 = []
#each game : [ [[...player1_1stgame], [...player2_1stgame]], [[...player1_2ndgame], [...player2_2ndgame]],  ...]
games = []

def beginTime(player):
    global lastMeasure_player, lastMeasure_beginTime
    lastMeasure_player = player
    lastMeasure_beginTime = time.time()*1000 #TODO perf prbm ?

def endTime(player):
    global lastMeasure_player, lastMeasure_beginTime, player1, player2
    if lastMeasure_player != player :
        print("Wow, cross time error, should not exist")

    lastMeasure_player = -1 #waiting new turn
    elapsedTime = (time.time()*1000) - lastMeasure_beginTime

    if player == 0 :
        player1 += [elapsedTime]
    else :
        player2 += [elapsedTime]

    return elapsedTime #in ms

def commitTime():
    global player1, player2, games
    games += [[player1, player2]] #TODO do not need a deepcopy ??
    player1 = []
    player2 = []

#getter
def lastGameTime(): #TODO implement
    return 4.2 #in ms

def globalStatsTime(): #faster version than calling individual max, min, avg
    global games
    print("Global stats")

    #init
    max_player1 = games[0][0][0] #reminder : game 0, player 0, turn 0
    min_player1 = games[0][0][0]
    nbr_player1 = 0 #number of turns
    tot_player1 = 0 #total time of playing
    avg_player1 = None

    max_player2 = games[0][1][0] #reminder : game 0, player 0, turn 0
    min_player2 = games[0][1][0]
    nbr_player2 = 0 #number of turns
    tot_player2 = 0 #total time of playing
    avg_player2 = None

    #parsing
    for i in range(len(games)) : # games
        for p in [0, 1] : # player
            for g in range( len(games[i][p]) ) : #may differ from 1 between two players
                if p == 0 :
                    max_player1 = max([ max_player1, games[i][p][g] ])
                    min_player1 = min([ min_player1, games[i][p][g] ])
                    nbr_player1 += 1
                    tot_player1 += games[i][p][g]
                else : # p == 1
                    max_player2 = max([ max_player2, games[i][p][g] ])
                    min_player2 = min([ min_player2, games[i][p][g] ])
                    nbr_player2 += 1
                    tot_player2 += games[i][p][g]

    #compute
    avg_player1 = tot_player1 / nbr_player1
    avg_player2 = tot_player2 / nbr_player2

    #display and return
    print("Player 1 :")
    print("max time : " + str(max_player1) + " ms")
    print("min time : " + str(min_player1) + " ms")
    print("avg time : " + str(avg_player1) + " ms")

    print("Player 2 :")
    print("max time : " + str(max_player2) + " ms")
    print("min time : " + str(min_player2) + " ms")
    print("avg time : " + str(avg_player2) + " ms")

    #TODO return
    #TODO better looked function

#TODO graph of time for 1 game
