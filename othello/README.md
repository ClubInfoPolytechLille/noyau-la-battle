# Structure de votre bot

Il s'agit d'une classe du nom de votre bot.
Elle doit hérité de IAModels.
Il doit avoir obligatoirement 1 méthode:
 - jouer(self) qui renvoie un tuple (coordx,coordx) qui indique ou jouer son pion 
 
# Explication fichier

## iaModels

il s'agit en faite d'une classe mère.
Vos bot vont hériter de cette classe.
Cela permet de les obliger a avoir la fonction jouer et de mettre a jour sans que vous vous en souciez les différents parametre du jeu comme l'etat du plateau.

## ialea

il s'agit d'un bot qui joue aléatoire, idéal pour tester vos premiers bots et comprendre la stucture de communication avec le noyau.

## jeu

le noyau, il dispose des règles du jeu d'othello, et c'est lui qui s'occupe de la communication.
Jetez y un oeil ;)

## reversix100

permet de lancer 100 matchs entre 2 bots.
Modifier le à votre guise ;)

## array2d

C'est juste une classe pour faire des matrices (ce noyau est vieux), une matrice x[0,0] marcherais aussi bien
En gros elle sert juste pour jeu

 **A noter, un fichier memoire enregistre les parties**

**N'hésitez pas a poser vos questions et encore merci a Guillaume Dussine avec qui nous avons pu faire une première bataille entre nos deux bots il y a 2 ans (les ia était pas ouf mais j'ai gagné ;))**
**Merci à Axel pour sa contribution ! progressbar et l'inclusion du temps**
