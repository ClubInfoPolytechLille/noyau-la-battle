from array2d import Array2D

global SIZE_X
global SIZE_Y
global grid
global turn

"""
Exemple d'utilisation :

import viewer
viewer.view("memoire/data.txt")
"""

def clearGrid():
    global SIZE_X, SIZE_Y, grid

    for i in range (SIZE_X):
        for j in range(SIZE_Y):
            grid.setvalue(i,j,0)
    grid.setvalue(3,3,2)
    grid.setvalue(4,4,2)
    grid.setvalue(3,4,1)
    grid.setvalue(4,3,1)


def getSymbol(player):
    if player==0:
        return " "
    elif player==1:
        return "0"
    else:
        return "-"

def opponent(joueur):
    if joueur==1 :
        return 2
    else:
        return 1

def findChangedTiles(x0,y0,dx,dy,j):
    global SIZE_X, SIZE_Y, grid
    x=x0+dx
    y=y0+dy
    tmp=[]
    
    while grid.isvalue(x,y) and grid.getvalue(x,y)==opponent(j):
        tmp.append((x,y))
        x=x+dx
        y=y+dy
    
    if not grid.isvalue(x,y) or (grid.isvalue(x,y) and grid.getvalue(x,y)==0) or \
    abs(x0-x)==1 or abs(y0-y)==1:
        return [(-1,-1)]
    return tmp

def updateRows(x , y, joueur):
    global grid
    for i in range(-1,2):
        for j in range(-1,2):
            if(i!=0 or j!=0):
                tiles = findChangedTiles(x , y, i , j, joueur)
                for k in range(len(tiles)):
                    grid.setvalue(tiles[k][0], tiles[k][1], joueur)
            


def updateGrid(code):
    global grid, turn
    lettersList=list(code)
    joueur=int(lettersList[0])
    x=int(ord(lettersList[1])-ord('a'))
    y=int(lettersList[2])
    grid.setvalue(x,y,joueur)
    updateRows(x,y,joueur)
    turn +=1
    


def display():
    global SIZE_X, SIZE_Y, grid, turn
    print("Turn",turn)
    for i in range(SIZE_X):
        for j in range(SIZE_Y):
            print(getSymbol(grid.getvalue(i,j)) , end=" ")
        print()
    print()
    print()

def view(url):
    global SIZE_X, SIZE_Y, grid, turn
    SIZE_X=8
    SIZE_Y=8
    turn = 0
    grid = Array2D(SIZE_X,SIZE_Y)
    
    fichier = open(url, "r")
    string = fichier.readline()
    moves=string.split(":")
    moves.pop(0)
    moves.pop(-1)
    tmp = moves[0]
    tmp=tmp.split("_")
    winner=tmp[0]
    moves[0]=tmp[1]
    
    clearGrid()
    for i in range(len(moves)):
        updateGrid(moves[i])
        display()
    print("Gagnant : Joueur",winner)

